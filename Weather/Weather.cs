﻿using System.Diagnostics;

namespace Weather
{
    internal static class Weather
    {
        private record Request(IReadOnlyList<WeatherEvent> EventData);
        private record Response(string Answer, string ElapsedTime);
        
        
        private static void Main(string[] args)
        {
            IReadOnlyList<WeatherEvent> weatherEvents = ReadData();

            var tasks = new List<Func<Request, Response>>
            {
                Task0,
                Task1,
                Task2,
                Task3,
                Task4,
                Task5,
                Task6,
            };
            
            var sw = new Stopwatch();
            sw.Start();
            foreach (var task in tasks)
            {
                var response = task.Invoke(new Request(weatherEvents));
                Console.WriteLine(response.Answer);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(response.ElapsedTime);
                Console.ResetColor();
                Console.WriteLine();
            }
            sw.Stop();
            Console.ForegroundColor = ConsoleColor.Red;
            var timeForeach = sw.Elapsed;
            Console.WriteLine($"\nForeach: {ParseTime(timeForeach)}\n");
            Console.ResetColor();
            
            sw = new Stopwatch();
            sw.Start();
            Parallel.ForEach(tasks, task =>
                {
                    var response = task.Invoke(new Request(weatherEvents));
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(response.ElapsedTime);
                }
            );
            sw.Stop();
            Console.ForegroundColor = ConsoleColor.Red;
            var timeParallelForeach = sw.Elapsed;
            Console.WriteLine($"\nParallel.ForEach: {ParseTime(timeParallelForeach)}\n");
            Console.ResetColor();


            var tasksArray = new Task[6];
            for (var i = 0; i < 6; ++i)
            {
                tasksArray[i] = new Task(() =>
                {
                    var response = tasks[i].Invoke(new Request(weatherEvents));
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(response.ElapsedTime);
                });
            }
            sw = new Stopwatch();
            sw.Start();
            foreach (var task in tasksArray)
            {
                task.Start();
            }
            Task.WaitAll(tasksArray);
            sw.Stop();
            Console.ForegroundColor = ConsoleColor.Red;
            var timeTasks = sw.Elapsed;
            Console.WriteLine($"\nTasks: {ParseTime(timeTasks)}\n");
            Console.ResetColor();

            Console.WriteLine(
                $"Optimization Tasks/Foreach(%): {100 - Math.Round(timeTasks.TotalMilliseconds / timeForeach.TotalMilliseconds * 100)}%");
            Console.WriteLine(
                $"Optimization Parallels/Foreach(%): {100 - Math.Round(timeParallelForeach.TotalMilliseconds / timeForeach.TotalMilliseconds * 100)}%");
        }

        /// <summary>
        /// Time formatting.
        /// </summary>
        /// <param name="time">Time.</param>
        /// <returns>Formatted string.</returns>
        private static string ParseTime(TimeSpan time)
        {
            return $"{time.Seconds}s {time.Milliseconds}ms";
        }

        private static Response Task0(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var weatherEventsCount = request.EventData.Count(el => el.StartTime.Year == 2018);
            var taskAnswer = $"Task 0 (answer): {weatherEventsCount} weather events.";
            sw.Stop();
            return new Response(taskAnswer, $"Task 0 (time): {ParseTime(sw.Elapsed)}");
        }
        
        private static Response Task1(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var cityCount = request.EventData.Select(el => el.City).Distinct().Count();
            var stateCount = request.EventData.Select(el => el.State).Distinct().Count();
            var taskAnswer = $"Task 1 (answer): {cityCount} cities, {stateCount} states.";
            sw.Stop();
            return new Response(taskAnswer, $"Task 1 (time): {ParseTime(sw.Elapsed)}");
        }

        private static Response Task2(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var taskAnswer = "Task 2 (answer):\n" + string.Join("\n",
                request.EventData
                    .Where(el => el.StartTime.Year == 2019 && el.Type == EventType.Rain)
                    .GroupBy(el => (el.City, el.Type))
                    .Select(el => (el.Key.City, Rain: el.Count()))
                    .OrderByDescending(el => el.Rain)
                    .Take(3)
                    .Select(el => $"City: {el.City}, Rains: {el.Rain}")) + ";";
            sw.Stop();
            return new Response(taskAnswer, $"Task 2 (time): {ParseTime(sw.Elapsed)}");
        }
        
        private static Response Task3(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var taskAnswer = "Task 3 (answer):\n" + string.Join("\n",
                request.EventData
                    .Where(el => el.Type == EventType.Snow)
                    .GroupBy(el => el.StartTime.Year)
                    .Select(el => 
                        (Year: el.Key, OrderedEvenets: el.OrderByDescending(el => el.EndTime - el.StartTime).First()))
                    .Select(el => 
                        $"Year: {el.Year}, " +
                        $"City: {el.OrderedEvenets.City}, " +
                        $"StartTime: {el.OrderedEvenets.StartTime}, " +
                        $"EndTime: {el.OrderedEvenets.EndTime}")
            );
            sw.Stop();
            return new Response(taskAnswer, $"Task 3 (time): {ParseTime(sw.Elapsed)}");
        }
        
        private static Response Task4(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var taskAnswer = "Task 4 (answer):\n" + string.Join("\n",
                request.EventData
                    .Where(el => el.StartTime.Year == 2019)
                    .OrderBy(el => el.StartTime)
                    .TakeWhile(el => el.EndTime - el.StartTime <= TimeSpan.FromHours(2))
                    .GroupBy(el => (State: el.State, EventCount: el.State.Count()))
                    .Select(el => $"State: {el.Key.State}, EventCount: {el.Key.EventCount}")
            );
            sw.Stop();
            return new Response(taskAnswer, $"Task 4 (time): {ParseTime(sw.Elapsed)}");
        }
        
        private static Response Task5(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var taskAnswer = "Task 5 (answer):\n" + string.Join("\n",
                request.EventData
                    .Where(el => el.StartTime.Year == 2017 && el.Severity == EventSeverity.Severe)
                    .GroupBy(el => (el.State, el.City))
                    .Select(el => 
                        (
                            State: el.Key.State, 
                            City: el.Key.City, 
                            CityTime: el.Aggregate(new TimeSpan(0), (total, el) => el.EndTime - el.StartTime))
                    )
                    .OrderByDescending(el => el.CityTime)
                    .DistinctBy(el => el.State)
                    .Select(el => $"State: {el.State}, City: {el.City}, Hours: {Math.Round(el.CityTime.TotalHours)}")
            );
            sw.Stop();
            return new Response(taskAnswer, $"Task 5 (time): {ParseTime(sw.Elapsed)}");
        }
        
        private static Response Task6(Request request)
        {
            var sw = new Stopwatch();
            sw.Start();
            var data = request.EventData
                .GroupBy(el => (el.StartTime.Year, el.Type))
                .Select(el =>
                    (
                        Year: el.Key.Year,
                        Type: el.Key.Type,
                        TypeCount: el.Count(),
                        AverageTime: Math.Round(el.Average(el => (el.EndTime - el.StartTime).TotalHours), 3)
                    )
                );
            var taskAnswer = "Task 6 (answer):\nMost frequent:\n" + string.Join("\n",
                data
                    .OrderByDescending(el => el.TypeCount)
                    .DistinctBy(el => el.Year)
                    .Select(el =>
                        $"Year: {el.Year}, Type: {el.Type}, Count: {el.TypeCount}, AverageTime: {el.AverageTime}h"));
            taskAnswer += "\nThe rarest:\n" + string.Join("\n",
                data
                    .OrderBy(el => el.TypeCount)
                    .DistinctBy(el => el.Year)
                    .Select(el =>
                        $"Year: {el.Year}, Type: {el.Type}, Count: {el.TypeCount}, AverageTime: {el.AverageTime}h"));
            sw.Stop();
            return new Response(taskAnswer, $"Task 6 (time): {ParseTime(sw.Elapsed)}");
        }
        
        /// <summary>
        /// Reading data from a file.
        /// </summary>
        private static IReadOnlyList<WeatherEvent> ReadData()
        {
            var weatherEvents = new List<WeatherEvent>();
            using var reader = new StreamReader("WeatherEvents_Jan2016-Dec2020.csv");
            // Reading header.
            var line = reader.ReadLine();
            while ((line = reader.ReadLine()) != null)
            {
                var eventUnit = line.Split(',');
                var typeEvent = (EventType) Enum.Parse(typeof(EventType), eventUnit[1]);
                var severityEvent = (EventSeverity) Enum.Parse(typeof(EventSeverity), eventUnit[2]);
                var startTimeEvent = DateTime.Parse(eventUnit[3]);
                var endTimeEvent = DateTime.Parse(eventUnit[4]);
                var locationLatEvent = double.Parse(eventUnit[7]);
                var locationLngEvent = double.Parse(eventUnit[8]);
                weatherEvents.Add(
                    new WeatherEvent(
                        eventUnit[0],
                        typeEvent,
                        severityEvent,
                        startTimeEvent,
                        endTimeEvent,
                        eventUnit[5],
                        eventUnit[6],
                        locationLatEvent,
                        locationLngEvent,
                        eventUnit[9],
                        eventUnit[10],
                        eventUnit[11],
                        eventUnit[12]
                    )
                );
            }

            return weatherEvents;
        }

        private record WeatherEvent(
            string EventId,
            EventType Type,
            EventSeverity Severity,
            DateTime StartTime,
            DateTime EndTime,
            string TimeZone,
            string AirportCode,
            double LocationLat,
            double LocationLng,
            string City,
            string Country,
            string State,
            string ZipCode
        );
        
        private enum EventType
        {
            UNK = 0,
            Cold = 1,
            Fog = 2,
            Hail = 3,
            Precipitation = 4,
            Rain = 5,
            Snow = 6,
            Storm = 7
        }

        private enum EventSeverity
        {
            UNK = 0,
            Heavy = 1,
            Light = 2,
            Moderate = 3,
            Severe = 4,
            Other = 5
        }
    }
}